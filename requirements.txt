numpy
scipy
pandas
seaborn

# User tools
ipython

# Documentation
matplotlib
Sphinx
sphinx-gallery
sympy
