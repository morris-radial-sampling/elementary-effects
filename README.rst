=======================================
Sensitivity Analysis with Morris method
=======================================

Friendly implementation of the Morris method.

Introduction
------------

TODO

Installation
------------
.. hint::
    To avoid collisions with your system's module versions,
    use a python virtual environment for installation.

To install the module using pip run the following command::

    pip install "git+https://gitlab.switch.ch/morris-radial-sampling/elementary-effects.git"

You can check the installation of the module by running::

    python -c 'import samorris as sm; sm.describe()'

which will print some basic information about the module.

Documentation
-------------

The latest documentation is hosted online at https://morris-radial-sampling.pages.switch.ch/elementary-effects/

Development
-----------
If you plan to extend the module or provide patches for it, you need to clone the
repository at https://gitlab.switch.ch/morris-radial-sampling/elementary-effects using git.
Once you have cloned the repository, upgrade and install all development dependencies
by running::

    pip install -U pip wheel
    pip install -U -r requirements.txt

in the root folder of the cloned repository (your working copy).

Building the documentation
^^^^^^^^^^^^^^^^^^^^^^^^^^

The sources for the documentation are located in the ``doc`` folder.
To build the documentation locally, run::

    cd doc
    make html

the documentation will then be located in the ``doc/build/html`` folder.
