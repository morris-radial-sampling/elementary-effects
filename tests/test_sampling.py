import unittest
import numpy as np
import samorris.sampling


class TestSamplingFunc(unittest.TestCase):

    def test_wrong_dimensional_delta_vonneumann(self):
        # Assertion
        with self.assertRaisesRegex(ValueError,
                                    r"length of delta does not match provided x_dim"):
            samorris.sampling.vonneumann_neighborhood(4, np.ones(5))

    def test_trajectory_missing_kwargs(self):
        # Assertion
        with self.assertRaisesRegex(KeyError,
                                    r"'ranges' and 'n_trajectories' must be "
                                    r"included in the keyword arguments."):
            delta = np.ones(3)
            ranges = np.array([[1, 2], [4, 6], [2, 4]])
            samorris.sampling.trajectory(None, delta=delta, ranges=ranges)

    def test_trajectory_wrong_ranges_dim(self):
        # Assertion
        with self.assertRaisesRegex(ValueError,
                                    r'incorrect shaped ranges.*'):
            delta = np.ones(5)
            ranges = np.array([[1, 2], [4, 6], [2, 4]])
            samorris.sampling.trajectory(None, delta=delta, ranges=ranges,
                                         n_trajectories=50)


if __name__ == '__main__':
    unittest.main()
