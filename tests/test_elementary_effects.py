import unittest
import numpy as np
from samorris.elementary_effects import elementary_effects


class TestElementaryEffectsFunc(unittest.TestCase):

    def test_signature(self):
        # Assertion to be checked
        with self.assertRaisesRegex(ValueError,
                                    r"Method 'wrong' is not supported"):
            # test method input
            elementary_effects(model=lambda x: 0, seeds=np.zeros((10, 3)),
                               method='wrong', delta=0)

        with self.assertRaisesRegex(TypeError,
                                    r"required keyword-only argument: 'delta'"):
            # test delta provided
            elementary_effects(model=lambda x: 0, seeds=np.zeros((10, 3)),
                               method='radial')


if __name__ == '__main__':
    unittest.main()
