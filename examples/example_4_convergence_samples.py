"""
===========================================================================
4) Elementary effects: number of samples convergence
===========================================================================

In this example, we look at the convergence of summary statistics of the
elementary effects calculated by the implemented radial and trajectory
methods when we increase the number of samples.

.. contents:: Contents

"""

# Import all the modules needed
import seaborn as sns
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt

import samorris.sampling as smsampling
import samorris.elementary_effects as smee
from examples.s_showcase_functions import Ishigami
from samorris.utils import mu_star

# %%
# Ishigami function
# ---------------------------------
# We can load the Ishigami function from our showcase functions. The
# Ishigami funtion is often used as a showcase for sensitivity analysis
# methods since it displays strong nonlinearity and nonmonotonicity. It has
# two parameters (often named a and b) that are usually fixed for analyses
# while three parameters (x_1-x_3 or x_0-x_2 ) are variable.
ish = Ishigami()
ish

# %%
# Reference values
# -----------------
# We choose to analyze the convergence of two metrics. One is the mean of the
# absolute values of the elementary effects, commonly called μ*. The other
# one is the absolute histogram difference. As "true" / reference value for
# the convergence we calculate the analytical partial derivatives from a
# large sample.
#
parameter_dimensionality = len(ish.sym_vars)
rng = np.random.default_rng()
true_seeds = -np.pi + 2 * np.pi * rng.uniform(size=(int(1e7),
                                                    parameter_dimensionality))
true_ee = ish.gradient_table(true_seeds)
true_mean = mu_star(true_ee)


# %%
# To get histogram distances we first need a reference histogram made of
# the true values. We arbitrarily choose 20 bins.
n_bins = 20
binedges = []
binwidth = np.zeros(parameter_dimensionality)
for ii in range(parameter_dimensionality):
    __, histoedges = np.histogram(true_ee.loc[:, f'w.r.t. x{ii}'], bins=n_bins)
    binedges.append(histoedges)
    binwidth[ii] = np.diff(histoedges)[0]


# %%
# Convergence analysis
# ---------------------
# In this section we will conduct the convergence analysis.
#
# Before we start calculating, we choose the sample numbers we want to
# loop through, initialize storage arrays for the results and set any
# analysis values that stay constant. For the storage initialization,
# the number 3 comes from the 2 methods plus analytical values.
seed_loop = [int(s) for s in np.logspace(0, 5, 15)]

sumstat_store = np.zeros((3 * parameter_dimensionality, len(seed_loop)))
hist_dist = np.zeros((len(seed_loop), 3, parameter_dimensionality))

step_size = 1
delta = np.array(parameter_dimensionality * [step_size])

# %%
# Now we will loop over the number of seeds. For each sample number,
# we first produce the seeds and then calculate the current elementary
# effects with both methods as well as the analytical gradients. From those
# we can get μ*.
#
# To get the histogram distance, we need to do construct comparable
# histograms. Basis for the histograms of the current elementary effects
# are the true bin edges from above. If there are points outside these
# edges, we extend the histogram using the same bin width. When the
# histogram edges include all data we can construct comparable histograms and
# calculate the distance between them.

for ii, n_seeds in enumerate(seed_loop):
    # produce seeds
    seeds_radial = -np.pi + 2 * np.pi * \
                   rng.uniform(size=(n_seeds, parameter_dimensionality))
    seeds_traj = np.vstack((seeds_radial, seeds_radial))

    # get elementary effects
    ee_radial = smee.elementary_effects(model=ish,
                                        seeds=seeds_radial,
                                        method='radial',
                                        delta=delta)

    ee_traj = smee.elementary_effects(model=ish,
                                      seeds=seeds_traj,
                                      method='trajectory',
                                      delta=delta)

    ee_analy = ish.gradient_table(seeds_radial)

    # compute and store summary statistic
    sumstat_store[0:3, ii] = np.mean(np.abs(ee_radial.ee), axis=(0, 1))
    sumstat_store[3:6, ii] = np.mean(np.abs(ee_traj.ee), axis=0)
    sumstat_store[6:, ii] = np.mean(np.abs(np.array(ee_analy)), axis=0)

    # histogram distances
    for jj in range(3):
        # extend histogram edges if needed
        ext_edges = binedges[jj]
        compare_traj = ee_traj.ee[:, jj]
        compare_radial = ee_radial.ee[:, :, jj]
        min_x = np.min([np.min(compare_traj), np.min(compare_radial)])
        max_x = np.max([np.max(compare_traj), np.max(compare_radial)])
        if np.min(ext_edges) > min_x:
            step_low = np.ceil((np.min(ext_edges) - min_x) / binwidth[jj])
            ext_low = np.arange(step_low) * -binwidth[jj]
            ext_low = np.min(ext_edges) + ext_low[1:][::-1]
            ext_edges = np.concatenate((ext_low, ext_edges))
        if np.max(ext_edges) < max_x:
            step_high = np.ceil((max_x - np.max(ext_edges)) / binwidth[jj])
            ext_high = np.arange(step_high + 1) * binwidth[jj]
            ext_high = np.max(ext_edges) + ext_high[1:]
            ext_edges = np.concatenate((ext_edges, ext_high))

        # build comparable histograms
        true_dens, __ = np.histogram(true_ee.loc[:, f'w.r.t. x{jj}'],
                                     bins=ext_edges, density=True)
        traj_dens, __ = np.histogram(compare_traj,
                                     bins=ext_edges, density=True)
        radial_dens, __ = np.histogram(compare_radial,
                                       bins=ext_edges, density=True)
        analy_dens, __ = np.histogram(ee_analy.loc[:, f'w.r.t. x{jj}'],
                                      bins=ext_edges, density=True)
        # calculate and store distance
        hist_dist[ii, 0, jj] = np.sum(np.abs(true_dens - traj_dens))
        hist_dist[ii, 1, jj] = np.sum(np.abs(true_dens - radial_dens))
        hist_dist[ii, 2, jj] = np.sum(np.abs(true_dens - analy_dens))

# %%
# Convergence plots
# -----------------
# In this section we plot and discuss the results of the analyses. The first
# plots show the convergence of the mean of the absolute elementary effects
# μ* for the different investigated parameters x_0, x_1, x_2. The following
# plot displays the convergence of the histogram distance. Its calculation
# is explained in the section above.
#
#
# Summary statistic
# *****************
# The following three plots show the convergence of μ* for x_0-x_2. On the
# x-axis we see the number of samples used to calculate the elementary
# effects and on the y-axis the value of μ*.
#
# The dashed black line shows the true value computed at the start of this
# example. The colored lines represent the results from the two elementary
# effect methods and the results from the analytical gradient at the
# generated seeds. Additionally, the step size we used for the
# elementary effects methods is displayed for clarity.

for vv in range(parameter_dimensionality):
    fig = plt.figure()
    plt.hlines(true_mean[vv], seed_loop[0], seed_loop[-1],
               label=f'true', colors='k', linestyles='dashed')
    for ww in [vv, vv + 3, vv + 6]:
        con, = plt.plot(seed_loop, sumstat_store[ww, :])
        if ww == vv:
            con.set_label(f'radial')
        elif ww == vv + 3:
            con.set_label(f'trajectory')
        elif ww == vv + 6:
            con.set_label(f'analytical')
    plt.semilogx()
    plt.ylabel(f'μ*_{vv}')
    plt.xlabel('number of samples')
    ax = plt.gca()
    ax.text(0.5, 0.05, f'step size={step_size}', fontsize=10,
            transform=ax.transAxes, ha='center')
    plt.legend()

# %%
# The plots allow for several interesting observations. For large number of
# samples all of them seem to converge. However, they do not always
# converge towards the same value. This can be explained with our large step
# size: the average (absolute) slope of our showcase function (μ* in other
# words) over a step width of 1 is very unlikely going to be equal to the
# average of the actual gradient.
#
# While the analytical values always converge towards the reference value, we
# can see that the results from the radial and trajectory methods not always
# converge towards the same value. In fact, for x_0 the radial methods
# results track the analytical ones closely and finally converge towards a
# much closer value than the trajectory methods results. Also, for x_2 the
# radial results track the analytical values but end up converging to the
# same value as the trajectory ones.
#
# Two final comments on these plots: one, especially the plots for x_0 show
# that a large number of samples does not guarantee the convergence of μ*
# towards its analytical equivalent. And two, they give us little info on
# the performance of the methods with small number of samples, which is how
# the Morris method is often used.


# %%
# Histogram distances
# ********************
# The following plots show the results of the convergence analysis of the
# distance between the histogram of the elementary effects with increasing
# sample size and the histogram of the gradients with from a large sample.
# This is an extension of the analysis done in the histogram visualization
# example with the Ishigami function.
#
# The x-axis again shows the number of samples used while the y-axis shows
# the histogram difference. Same as before, the colored lines show the
# results from the two elementary effects methods and the analytical
# gradients at the seeds. Ideally, they would approach zero.

for vv in range(parameter_dimensionality):
    plt.figure()
    for mm in range(parameter_dimensionality):
        if mm == 0:
            cc = 'blue'
            met = 'trajectory'
        elif mm == 1:
            cc = 'red'
            met = 'radial'
        elif mm == 2:
            cc = 'green'
            met = 'analytical'
        ax, = plt.plot(seed_loop, hist_dist[:, mm, vv], color=cc)
        plt.hlines(0, -1e2, 1e5, color='k', linewidth=0.5)
        plt.xlabel('number of samples')
        plt.ylabel(f'histogram distance')
        plt.title(f'Discrepancy of elementary effects histogram of x{vv}')
        ax.set_label(met)
        plt.legend()
        plt.semilogx()
    plt.show()

# %%
# The convergence behaviour in these plots is similar to the ones on the
# summary statistics. Again we can see, that for our chosen step size the
# discrepancies converge towards values that do not necessarily equal the
# true - in our case the analytical - ones.
