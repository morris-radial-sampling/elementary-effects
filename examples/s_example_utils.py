"""
    Snippets useful for examples.
"""

# Copyright (C) 2021 Juan Pablo Carbajal
#
# This program is free software; you can redistribute it and/or modify it under
# the terms of the GNU General Public License as published by the Free Software
# Foundation; either version 3 of the License, or (at your option) any later
# version.
#
# This program is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
# FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
# details.
#
# You should have received a copy of the GNU General Public License along with
# this program; if not, see <http://www.gnu.org/licenses/>.

# Author: Juan Pablo Carbajal <ajuanpi+dev@gmail.com>

import matplotlib.pyplot as plt


def maximize_figure():
    """ Resize the current figure to screen size. """
    mng = plt.get_current_fig_manager()
    try:
        mng.resize(*mng.window.maxsize())
    except AttributeError:
        # If there is no window, don't do anything
        pass


