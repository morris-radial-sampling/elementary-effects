"""
====================================================================
2) Elementary effects: histogram visualization of Ishigami function
====================================================================

In this example, we compare the elementary effects calculated with the
implemented radial and trajectory methods. The elementary effects are
plotted as histograms next to each other.
In this example we work with the Ishigami function, a nonlinear and
nonmonotonic function that is often used as a showcase in sensitivity analysis.

.. contents:: Contents
.. note::
    This example is similar to the histogram visualization of the ReLU
    function. The difference lies in the showcase function and the
    discussion of the results.

"""

# Import all the modules needed
import seaborn as sns
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt

from samorris.elementary_effects import elementary_effects
from samorris.utils import results_to_df, mu_star
from examples.s_showcase_functions import Ishigami

# %%
# Ishigami function
# ---------------------------------
# We can load the Ishigami function from our showcase functions. The
# Ishigami funtion is often used as a showcase for sensitivity analysis
# methods since it displays strong nonlinearity and nonmonotonicity. It has
# two parameters (often named a and b) that are usually fixed for analyses
# while three parameters (x_1-x_3 or x_0-x_2 ) are variable.
ish = Ishigami()
ish

# %%
# Set seeds
# -----------
# Now we will set the number of seeds to calculate the elementary effects
# with the two methods to get comparable results. With the implemented
# algorithms, the radial method produces twice the number of elementary
# effects per seed compared to the trajectory method. To compensate for
# this, the trajectory method get the same seeds twice.
#
# We intentionally choose a large step size to emphasize the differences the
# methods produce.
parameter_dimensionality = len(ish.sym_vars)
ranges = np.array([[-np.pi, np.pi]] * parameter_dimensionality)
n_seeds_rad = 100
delta = np.array([1] * parameter_dimensionality)

rng = np.random.default_rng()
seeds_radial = -np.pi + delta + 2 * (np.pi - delta) * \
               rng.uniform(size=(n_seeds_rad, parameter_dimensionality))
seeds_traj = np.vstack((seeds_radial, seeds_radial))

# %%
# Elementary effects
# ------------------
# Following, we simply apply the modules on the generated seeds to get the
# elementary effects. To get a reference point, we also evaluate the analytical
# gradient of our showcase function at the seeds.

ee_radial = elementary_effects(model=ish,
                               seeds=seeds_radial,
                               method='radial',
                               delta=delta)

ee_traj = elementary_effects(model=ish,
                             seeds=seeds_traj,
                             method='trajectory',
                             delta=delta)

ee_anal = ish.gradient_table(seeds_traj)
ee_anal = np.array(ee_anal)

# %%
# Construct dataframes from results
# **************************************
# To more easily plot our elementary effects using seaborn, we now convert
# the results from our analyses from ndarrays into dataframes. To check out
# this utility function see :mod:`utils.results_to_df`

df_rad = results_to_df(ee_radial.ee, 'radial')
df_traj = results_to_df(ee_traj.ee, 'trajectory')
df_anal = results_to_df(ee_anal, 'trajectory')
df_anal.loc[:, 'method'] = 'analytical'

# %%
# To concatenate the dataframes we need to drop the "step direction" column
# from the radial dataframe.
#
df_rad_ = df_rad.drop('step_direction', axis=1)
df_all = pd.concat((df_traj, df_rad_, df_anal), axis=0, ignore_index=True)


# %%
# Results
# ---------
# Now we are ready to display and compare the results of our analyses. We
# print summary statistics of the elementary effects in a table and further
# down plot histograms of the elementary effects to show the differences.

# %%
# Summary statistics
# **********************
# As summary statistics we look at the ones originally proposed by Morris
# and later by Campolongo et al., namely the mean, the mean of the absolute
# values μ* and the standard deviation σ of the elementary effects. The results
# are displayed in the following table:
summary = df_all.groupby(['variable', 'method']).agg(['mean', mu_star, np.std])
summary.rename(columns={'mu_star': 'μ*', 'sigma': 'σ'}, level=1).round(2)


# %%
# Several observations can be made here: We can see how the values of the
# mean and μ* differ. This is due to negative values the elementary effects
# can take. It has been well discussed in literature why it is beneficial
# to work with μ* as a summary statistic.
#
# Overall the values produced by the two methods lie relatively close to the
# analytical values. While we note that in this case the radial method gives
# values closer to the analytical ones than the trajectory method, we want
# to mention that it is not possible to judge if one method outperforms the
# other one from one analysis alone.
#
#
# Histograms
# **********
#
# With the elementary effects plotted as histograms we get further
# information. Overall, we can see that the elementary effects produced by
# the methods are distributed similarly as the actual partial derivatives at
# the investigated seeds. Obviously, this depends mainly on the chosen step
# size.
x_var = 'elementary_effect'
for curr_var, curr_data in df_all.groupby('variable'):
    plt.figure()
    plt.title(curr_var)
    sns.histplot(data=curr_data, multiple='dodge', shrink=0.7, stat='percent',
                 common_norm=False, x=x_var, hue='method', bins=20)

# %%
# The smaller the step size, the better the approximation by the
# methods. With the trajectory method large steps additionally lead to
# further deviation from the true values since after the first elementary
# effects estimation we use a point "far" away from the seed for the next
# estimation. The radial method does not have this shortcoming. This is the
# basic difference between these two method.
#
# This difference is investigated in the convergence analyses. There we will
# look at how sample and step size influence the accuracy of the
# approximations.
