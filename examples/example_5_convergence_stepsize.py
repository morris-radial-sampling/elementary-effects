"""
==========================================================
5) Elementary effects: step size convergence
==========================================================

In this example, we check the error of the local partial derivatives
calculated with the two methods and decreasing step sizes.


.. contents:: Contents

"""
# Import all the modules needed
from itertools import product

import numpy as np
import pandas as pd
import matplotlib.pyplot as plt

import samorris.elementary_effects as smee
from examples.s_showcase_functions import Ishigami

# %%
# Ishigami function
# ---------------------------------
# We can load the Ishigami function from our showcase functions. The
# Ishigami funtion is often used as a showcase for sensitivity analysis
# methods since it displays strong nonlinearity and nonmonotonicity. It has
# two parameters (often named a and b) that are usually fixed for analyses
# while three parameters (x_1-x_3 or x_0-x_2 ) are variable.
ish = Ishigami()
ish

# %%
# Varying step sizes
# ----------------------
# For small step sizes, the elementary effects calculated by both methods
# should approach the analytical values of the partial derivatives

# %%
# Setup
# ******
# We choose the seed [2, -2, 1] (a point in the input space of the model)
# and calculate the analytical derivatives there. We define decreasing step
# sizes and set up dataframes for the elementary effects.
seeds = np.array([[2, -2, 1]])
analy_ee = ish.gradient_table(seeds)

step_sizes = [1, 0.5, 0.1, 0.01, 0.001, 0.0001]

var_names = [f'{x}' for x in ish.sym_vars]
traj_gradients = pd.DataFrame(index=step_sizes, columns=var_names)
traj_gradients.index.name = 'step size'

signed_var_names = [f'{x}{s}' for x, s in product(var_names, ['+', '-'])]
radial_gradients = pd.DataFrame(index=step_sizes, columns=signed_var_names)
radial_gradients.index.name = 'step size'
# %%
# Calculate elementary effects
# *****************************
# Looping through the step sizes we calculate the elementary effects for each
# of them.
parameter_dimensionality = len(ish.sym_vars)
for step_size in step_sizes:
    delta = np.array(parameter_dimensionality * [step_size])
    ee_radial = smee.elementary_effects(model=ish,
                                        seeds=seeds,
                                        method='radial',
                                        delta=delta)
    ee_traj = smee.elementary_effects(model=ish,
                                      seeds=seeds,
                                      method='trajectory',
                                      delta=delta)

    radial_gradients.loc[step_size, :] = ee_radial.ee.flatten(order='F')
    traj_gradients.loc[step_size, :] = ee_traj.ee.flatten()

# %%
# Then we compute the difference to the analytical values of the partial
# derivatives in the given seed point
for v in analy_ee:
    v_ = v.split()[-1]
    msk = radial_gradients.columns.str.startswith(v_)
    cols = radial_gradients.columns[msk]
    cols_ = [f'error_{x}' for x in cols]
    radial_gradients[cols_] = (radial_gradients[cols].to_numpy() - analy_ee[
        v].to_numpy()) / analy_ee[v].to_numpy()

    cols_ = f'error_{v_}'
    traj_gradients[cols_] = (traj_gradients[v_].to_numpy() - analy_ee[
        v].to_numpy()) / analy_ee[v].to_numpy()

# %%
# Results
# ------------
# In this section we print the results in tables and plots. In the left
# column of the tables we see the step size and the content displays the
# difference between the elementary effects and the analytical derivatives.
# The radial method calculates elementary effects in
# the increasing and decreasing direction of the variables.
#
#
#
# Radial error table
# ******************
error_cols_rad = radial_gradients.columns[
    radial_gradients.columns.str.startswith('error')]
radial_gradients[error_cols_rad]

# %%
# Trajectory error table
# **********************
error_cols_traj = traj_gradients.columns[
    traj_gradients.columns.str.startswith('error')]
traj_gradients[error_cols_traj]

# %%
# Plots
# ******
# The numbers in the tables above are much easier to read when visualized.
# In the first two plots we can see error for the two methods.

fig, axes = plt.subplots(nrows=1, ncols=2, figsize=(10, 4.8))
ax = radial_gradients[error_cols_rad].plot(grid=True, ax=axes[0])
ax.set(title='radial method error')
ax.semilogx()
ax.invert_xaxis()
plt.ylabel('error [%]')
ax = traj_gradients[error_cols_traj].plot(grid=True, ax=axes[1])
ax.set(title='trajectory method error')
ax.semilogx()
ax.invert_xaxis()

# %%
# From the tables and plots above give us a good overview over the errors.
# We can see that the errors converge towards zero. This means the
# elementary effects from both methods do converge to the analytical values. We
# can see that in both cases at a step width of :math:`10^{-2}=0.01` we get
# errors in the order of :math:`10^{-2}`, which is usually sufficiently
# accurate of Morris analyses.
#
# Now we can also split plot the methods against each others. In the following
# we can see one plot for each variable showing the error convergence of the
# two methods:

for vv in range(parameter_dimensionality):
    fig = plt.figure()
    ax = plt.axes()
    rad_ax = ax.plot(radial_gradients.loc[:,
                     radial_gradients.columns.str.startswith(f'error_x{vv}')])
    traj_ax = ax.plot(traj_gradients.loc[:,
                      traj_gradients.columns.str.startswith(f'error_x{vv}')])
    ax.set(title=f'elementary effect x_{vv}')
    ax.semilogx()
    ax.invert_xaxis()
    plt.ylabel('error [%]')
    ax.legend([f'rad+', f'rad-', f'traj'])

# %%
# The above plots do not allow to draw any clear conclusions. The fact that
# the analysis was done with one point alone is a limiting factor here.
#
# In summary, we can state that we can clearly see that the implemented
# methods approximate the analytical values and approach them for decreasing
# step sizes. A clearly stronger performance of one method could not be
# found yet. For that, a combination of the two convergence examples would
# most probably yield better insight.
