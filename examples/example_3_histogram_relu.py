"""
================================================================
3) Elementary effects: histogram visualization of ReLU function
================================================================

In this example, we compare the summary statistics of the elementary effects
with their histogram.
In this example we work with the Rectified Linear Unit (ReLU) function. It
is a very simple function that will allow us to discuss the shortcomings of
summary statistics.

.. contents:: Contents
.. note::
    This example is similar to the histogram visualization of the Ishigami 
    function. The difference lies in the showcase function and the 
    discussion of the results.

"""

# Import all the modules needed
import seaborn as sns
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt

from samorris.elementary_effects import elementary_effects
from samorris.utils import results_to_df, mu_star
from examples.s_showcase_functions import RectifiedLinearUnit

# %%
# ReLU function
# ---------------------------------
# To showcase this convergence we load the ReLU function. It is a simple
# function that equals 0 for x < 0, and x otherwise. We will look at it in
# the range from -2 to 1.
relu = RectifiedLinearUnit()
relu

x_vals = np.array([[-2], [0], [1]])
fig = plt.figure()
ax = fig.add_subplot(111)
ax.plot(x_vals, relu(x_vals), label='ReLU')
for pos in ['left', 'bottom']:
    ax.spines[pos].set_position('zero')
for pos in ['right', 'top']:
    ax.spines[pos].set_visible(False)
ax.set_yticks(np.arange(0, 2, 0.5))
plt.xlabel('x', weight='bold', fontsize=12)
plt.ylabel('y', rotation='horizontal', weight='bold', fontsize=12)
plt.legend(frameon=False, fontsize=14, loc='upper left')
plt.show()

# %%
# Set seeds
# -----------
# Now we will set the number of seeds to calculate the elementary effects
# with the two methods to get comparable results. With the implemented
# algorithms, the radial method produces twice the number of elementary
# effects per seed compared to the trajectory method. To compensate for
# this, the trajectory method get the same seeds twice.
parameter_dimensionality = len(relu.sym_vars)
n_seeds_rad = 100
delta = np.array([1] * parameter_dimensionality)

rng = np.random.default_rng()
seeds_radial = -2 + 3 * \
               rng.uniform(size=(n_seeds_rad, parameter_dimensionality))
seeds_traj = np.vstack((seeds_radial, seeds_radial))

# %%
# Elementary effects
# ------------------
# Following, we simply apply the modules on the generated seeds to get the
# elementary effects. To get a reference point, we also evaluate the analytical
# gradient of our showcase function at the seeds.

ee_radial = elementary_effects(model=relu,
                               seeds=seeds_radial,
                               method='radial',
                               delta=delta)

ee_traj = elementary_effects(model=relu,
                             seeds=seeds_traj,
                             method='trajectory',
                             delta=delta)

ee_anal = relu.gradient_table(seeds_traj)
ee_anal = np.array(ee_anal)

# %%
# Construct dataframes from results
# **************************************
# To more easily plot our elementary effects using seaborn, we now convert
# the results from our analyses from ndarrays into dataframes. To check out
# this utility function see :mod:`utils.results_to_df`
#
df_rad = results_to_df(ee_radial.ee, 'radial')
df_traj = results_to_df(ee_traj.ee, 'trajectory')
df_anal = results_to_df(ee_anal, 'trajectory')
df_anal.loc[:, 'method'] = 'analytical'

# %%
# To concatenate the dataframes we need to drop the "step direction" column
# from the radial dataframe.
#
df_rad_ = df_rad.drop('step_direction', axis=1)
df_all = pd.concat((df_traj, df_rad_, df_anal), axis=0, ignore_index=True)


# %%
# Results
# ---------
# Now we are ready to display and compare the results of our analyses. We
# print summary statistics of the elementary effects in a table and further
# down plot the histogram of the elementary effects.

# %%
# Summary statistics
# **********************
# As summary statistics we look at the ones originally proposed by Morris
# and later by Campolongo et al., namely the mean, the mean of the absolute
# values μ* and the standard deviation σ of the elementary effects. The results
# are displayed in the following table:
summary = df_all.groupby(['variable', 'method']).agg(['mean', mu_star, np.std])
summary.rename(columns={'mu_star': 'μ*', 'sigma': 'σ'}, level=1).round(2)

# %%
# The summary statistics are discussed in the similar example on the histogram
# visualization with the Ishigami function.
# Obviously, there is only one parameter influencing the output of our
# investigated function. However, in this example we want to focus on the
# different conclusion about that influence we draw from the summary
# statistics and the histogram. This is discussed after the histogram plot.
#
#
# Histogram
# **********
# With the elementary effects plotted as a histogram we get further
# information.
x_var = 'elementary_effect'
for curr_var, curr_data in df_all.groupby('variable'):
    plt.figure()
    plt.title(curr_var)
    sns.histplot(data=curr_data, multiple='dodge', shrink=0.7, stat='percent',
                 common_norm=False, x=x_var, hue='method', bins=10)

# %%
#
# .. note ::
#   We might find some values in the histogram - even analytical ones - larger
#   than 1 or smaller than 0. The ReLU function can never have a slope
#   outside the range of 0 and 1. We suspect that this is an artifact from
#   our implementation of the function. We use the exponential function,
#   which around 0, leads to extreme values resulting in numerical
#   inaccuracies.
#
# We can see that are two spikes in the elementary effects of this
# parameter. This comes from the fact, that the function behaves very
# differently in different parts of the investigated space. The histogram
# tells us that, in fact, this parameter is noninfluential in some parts (when
# the elementary effects equal 0) and three times as influential as μ* would
# make us believe in other parts. By combining this information into one
# summary statistic such as μ*, we lose all this information.
#
# To summarize: this example shows us that for no additional cost, we can
# get additional information and avoid misleading information by having a
# look at the histograms of the elementary effects instead of only working
# with the summary statistics.
