"""
    Functions used to showcase the functionality in the library.
"""

# Copyright (C) 2021 Juan Pablo Carbajal
#
# This program is free software; you can redistribute it and/or modify it under
# the terms of the GNU General Public License as published by the Free Software
# Foundation; either version 3 of the License, or (at your option) any later
# version.
#
# This program is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
# FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
# details.
#
# You should have received a copy of the GNU General Public License along with
# this program; if not, see <http://www.gnu.org/licenses/>.

# Author: Juan Pablo Carbajal <ajuanpi+dev@gmail.com>
import numpy as np
import pandas as pd
from sympy import symbols, sin, exp, lambdify
from sympy.printing import pretty


class Function:

    def __init__(self, sym_func, variables):
        self.sym_func = sym_func
        self.sym_vars = variables
        self.sym_grad = [self.sym_func.diff(u) for u in self.sym_vars]
        self._lambda_func = lambdify(self.sym_vars, self.sym_func)
        self._lambda_grad = [lambdify(self.sym_vars, df) for df in self.sym_grad]

    def __call__(self, x) -> np.ndarray:
        try:
            return self._lambda_func(*x.T)
        except AttributeError:
            return self._lambda_func(x)

    def gradient(self, x) -> np.ndarray:
        try:
            return np.asarray([g(*x.T) for g in self._lambda_grad]).T
        except AttributeError:
            return np.asarray([g(x) for g in self._lambda_grad]).T

    def gradient_table(self, x):
        cols = [f'w.r.t. {x}' for x in self.sym_vars]
        return pd.DataFrame(data=self.gradient(x), columns=cols)

    def __repr__(self):
        return pretty(self.sym_func, use_unicode=True)

    #def _repr_html_(self):
    #    return "<div>" + pretty(self.sym_func) + "</div>"


class Ishigami(Function):

    a, b = symbols(r'a b', real=True)
    x = symbols(r'x:3', real=True)
    formula = sin(x[0]) * (1 + a * x[2] ** 4) + b * sin(x[1]) ** 2

    def __init__(self, *, a=1, b=1):
        params = {self.a: a, self.b: b}
        self.formula = self.formula.subs(params)
        super().__init__(self.formula, self.x)


class RectifiedLinearUnit(Function):

    a = symbols(r'a', real=True, negative=True)
    x = symbols(r'x', real=True)
    formula = x / (1 + exp(-a * x))

    def __init__(self, *, a=50):
        params = {self.a: a}
        self.formula = self.formula.subs(params)
        super().__init__(self.formula, [self.x])
