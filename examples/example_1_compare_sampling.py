"""
========================================
1) Basic comparing of samplings
========================================

In this dimple example, we compare the radial to the more traditional
trajectory or path sampling. Starting with the same seeds (i.e. starting
points) we will produce samples with both methods and visualize the results.

.. note::
    A basic implementations of the trajectory method is implemented here. The
    trajectories could be continued indefinitely by adding steps in random
    directions as long as the resulting points stay inside the sampling space.

"""

# Import all the modules needed
import numpy as np
import matplotlib.pyplot as plt

import samorris.sampling as sampling

# %%
# Sampling
# --------
# Following we produce the samples with the two aforementioned methods
#
# Set parameters
# ******************
# First we define the seeds, where from we will sample from with both
# methods. To facilitate visualization, we define 3 seeds in 3-dimensional
# space.
seeds = np.array([[-1.5, -2, -2],
                  [1.5, 0, -2],
                  [0, 1, 2]])

# %%
# We also define a unity delta (step width) in each dimension.
n_seeds, n_dim = seeds.shape
delta = 1.0 * np.ones(n_dim)

# %%
# Generate samples
# *****************
# Using the functions of the sampling module we can produce the samples for
# each method.
samples_radial = sampling.vonneumann_neighbors(seeds, delta)

samples_traj, __ = sampling.trajectory(seeds, delta=delta)

# %%
# Plot
# ----
#
# Following we define parameters for the plots, such as marker size and type
# and initiate the figure.
#
# Further down we plot the seeds and samples produced above.
seed_size = 100
neigh_size = seed_size - 5
seed_marker = '*'

fig = plt.figure(figsize=(10, 5))

ax = fig.add_subplot(1, 2, 1, projection='3d')
ax.scatter(seeds[:, 0], seeds[:, 1], seeds[:, 2],
           color='k', s=seed_size, marker=seed_marker, alpha=1, label='seeds')
ax.scatter(samples_radial[:, 1:4, 0], samples_radial[:, 1:4, 1], samples_radial[:, 1:4, 2],
           color='b', s=neigh_size, marker='+', alpha=1, label='samples in '
                                                               'pos. step '
                                                               'dir.')
ax.scatter(samples_radial[:, 4:, 0], samples_radial[:, 4:, 1], samples_radial[:, 4:, 2],
           color='r', s=neigh_size, marker='_', alpha=1, label='samples '
                                                               'in neg. '
                                                               'step dir')
for ii in range(samples_radial.shape[0]):
    for jj in range(1, samples_radial.shape[1]):
        curr_points = samples_radial[ii, [0, jj], :]
        if jj <= (samples_radial.shape[1] - 1) / 2:
            line_color = 'b'
        else:
            line_color = 'r'
        ax.plot(curr_points[:, 0], curr_points[:, 1], curr_points[:, 2], color=line_color)
ax.set_xlim(left=-3, right=3)
ax.set_ylim3d(-3, 3)
ax.set_zlim3d(-3, 3)
ax.set_xlabel('X')
ax.set_ylabel('Y')
ax.set_zlabel('Z')
ax.legend(fontsize='small')
ax.set_title('Radial sampling')

ax = fig.add_subplot(1, 2, 2, projection='3d')
ax.scatter(seeds[:, 0], seeds[:, 1], seeds[:, 2],
           color='k', s=seed_size, marker=seed_marker, alpha=1, label='seeds')
ax.scatter(samples_traj[:, 1:, 0], samples_traj[:, 1:, 1], samples_traj[:, 1:, 2],
           color='b', s=neigh_size, marker='.', alpha=1, label='samples')
for ii in range(samples_traj.shape[0]):
    ax.plot(samples_traj[ii, :, 0], samples_traj[ii, :, 1], samples_traj[ii, :, 2])
ax.set_xlim(left=-3, right=3)
ax.set_ylim3d(-3, 3)
ax.set_zlim3d(-3, 3)
ax.set_xlabel('X')
ax.set_ylabel('Y')
ax.set_zlabel('Z')
ax.legend(fontsize='small')
ax.set_title('Trajectory sampling')
plt.show()

# %%
# In the first subplot we can see the radial samples lying around the
# central seed.
#
# In the second subplot we can see the samples lying on trajectories
# starting from the seed.
