=================
API documentation
=================
.. contents:: :local:

Module: elementary_effects -- :mod:`samorris.elementary_effects`
=================================================================

.. automodule:: samorris.elementary_effects
   :members:
   :undoc-members:

Module: sampling -- :mod:`samorris.sampling`
========================================================

.. automodule:: samorris.sampling
   :members:
   :undoc-members:

Module: utils -- :mod:`samorris.utils`
===========================================

.. automodule:: samorris.utils
   :members:
   :undoc-members:
