.. Sensitivity Analysis with Morris sampling documentation master file, created by
   sphinx-quickstart on Thu Sep 30 10:10:18 2021.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to Sensitivity Analysis with Morris sampling's documentation!
=====================================================================

.. toctree::
   :maxdepth: 2
   :caption: Contents:

   overview

   API

   auto_examples/index.rst


Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
