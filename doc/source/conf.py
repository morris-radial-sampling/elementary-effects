# Configuration file for the Sphinx documentation builder.
#
# This file only contains a selection of the most common options. For a full
# list see the documentation:
# https://www.sphinx-doc.org/en/master/usage/configuration.html

# -- Path setup --------------------------------------------------------------

# If extensions (or modules to document with autodoc) are in another directory,
# add these directories to sys.path here. If the directory is relative to the
# documentation root, use os.path.abspath to make it absolute, like shown here.
#
import os
from pathlib import Path
import sys

from sphinx_gallery.sorting import FileNameSortKey

sys.path.insert(0, str(Path('../..').resolve()))
import samorris as sm

# -- Project information -----------------------------------------------------

project = 'Sensitivity Analysis with Morris method'
copyright = sm.__copyright__
author = sm.__author__

# The full version, including alpha/beta/rc tags
release = sm.__version__

# -- General configuration ---------------------------------------------------

# Add any Sphinx extension module names here, as strings. They can be
# extensions coming with Sphinx (named 'sphinx.ext.*') or your custom
# ones.
extensions = [
    'sphinx.ext.autodoc',
    'sphinx.ext.doctest',
    'sphinx.ext.viewcode',
    'sphinx.ext.napoleon',
    'sphinx.ext.intersphinx',
    'sphinx_gallery.gen_gallery',
]

# Add any paths that contain templates here, relative to this directory.
templates_path = ['_templates']

# List of patterns, relative to source directory, that match files and
# directories to ignore when looking for source files.
# This pattern also affects html_static_path and html_extra_path.
exclude_patterns = []

# -- Options for HTML output -------------------------------------------------

# The theme to use for HTML and HTML Help pages.  See the documentation for
# a list of builtin themes.
#
# html_theme = 'alabaster'
html_theme = 'sphinxdoc'

# Add any paths that contain custom static files (such as style sheets) here,
# relative to this directory. They are copied after the builtin static files,
# so a file named "default.css" will overwrite the builtin "default.css".
html_static_path = ['_static']

# -- Extension configuration -------------------------------------------------

# -- Options for Autodoc extension --------------------------------------------
# This value selects if automatically documented members are sorted alphabetic
# (value 'alphabetical'), by member type (value 'groupwise') or by source order
# (value 'bysource'). The default is alphabetical.
autodoc_member_order = 'groupwise'

# -- Options for Napoleon extension -------------------------------------------
napoleon_google_docstring = False
napoleon_numpy_docstring = True
napoleon_include_init_with_doc = False
napoleon_include_private_with_doc = False
napoleon_include_special_with_doc = True
napoleon_use_admonition_for_examples = False
napoleon_use_admonition_for_notes = False
napoleon_use_admonition_for_references = False
napoleon_use_ivar = False
napoleon_use_param = True
napoleon_use_rtype = False
# -- Options for Intersphinx extension ----------------------------------------
intersphinx_mapping = {
    'python': ('https://docs.python.org/3/', None),
    'numpy': ('https://numpy.org/doc/stable/', None),
    'scipy': ('https://docs.scipy.org/doc/scipy/reference', None),
    'matplotlib': ('https://matplotlib.org/', None),
}
# -- Options for Gallery extension --------------------------------------------
import re

sphinx_gallery_conf = {
    # path to your example scripts
    'examples_dirs': ['../../examples', ],
    # path to where to save gallery generated output
    # ideally goes to build, so it is clean up, but it doesn't work in the
    # index.rst
    # of the documentation
    # 'gallery_dirs': ['../build/auto_examples',],

    # files executed
    'filename_pattern': f'{re.escape(os.sep)}(?=example_)',

    # excluded files
    'ignore_pattern': f'{re.escape(os.sep)}s_|__init__\.py',

    # order of examples
    'within_subsection_order': FileNameSortKey
}
