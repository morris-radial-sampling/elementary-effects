from setuptools import setup, find_packages

with open('README.rst', encoding='utf-8') as f:
    long_description = f.read()

setup(name='samorris',
      version='1.0.dev0',
      python_requires='>=3.8.5',
      description='This package is allows to calculate the morris elementary effects using several sampling methods',
      long_description=long_description,
      long_description_content_type='text/x-rst',
      classifiers=[
        'Development Status :: early stage',
        'Programming Language :: Python :: 3',
        'Programming Language:: Python:: 3.9',
        'Programming Language:: Python:: 3.8',
        'Operating System:: OS Independent ',
        'License:: OSI Approved:: GNU General Public License v3(GPLv3)'],
      install_requires=['numpy'],
      packages=find_packages(exclude=["tests*"]),
      author='Philippe Wiederkehr',
      author_email='Philippe.Wiederkehr@eawag.ch',
      license='GNU GPLv3',
      url='https://gitlab.switch.ch/morris-radial-sampling/elementary-effects',
      project_urls =
            {'Bug Tracker': 'https://gitlab.switch.ch/morris-radial-sampling/elementary-effects/-/issues',
             'Documentation': 'https://morris-radial-sampling.pages.switch.ch/elementary-effects/'},
      )
