"""
    Utilities to calculate elementary effects
"""

# Copyright (C) 2021 Philippe Wiederkehr
#
# This program is free software; you can redistribute it and/or modify it under
# the terms of the GNU General Public License as published by the Free Software
# Foundation; either version 3 of the License, or (at your option) any later
# version.
#
# This program is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
# FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
# details.
#
# You should have received a copy of the GNU General Public License along with
# this program; if not, see <http://www.gnu.org/licenses/>.

# Author: Philippe Wiederkehr <philippe_wiederkehr@hotmail.com>
# Author: Juan Pablo Carbajal <ajuanpi+dev@gmail.com>

from collections import namedtuple
import numpy as np

from samorris import sampling
from samorris.utils import model_eval

ElementaryEffect = namedtuple('ElementaryEffect', ['delta_x', 'delta_y', 'ee'])
ElementaryEffect.__doc__ = "Container for elementary effects results."


def elementary_effects(model, seeds, *, method, delta, **kwargs):
    """ Compute the elementary effects of the given model using the chosen method.

        Arguments
        ---------
        model: callable
            A vectorized function. signature?

        seeds: list or array_like, shape (n_seeds, x_dim)
            if method is "radial", these are the centers. \n
            if method is "trajectory", these are the starting points. In this
            case, if seeds is None an automated optimal trajectories approach
            is used. Out of a large number of trajectories, the set with the
            best spread is chosen.

        method: str
            Valid methods are "trajectory" or "radial".

        delta: int or array_like, shape (x_dim, )
            Trajectory step width or neighborhood radius

        kwargs: dict
            Keyword arguments pass to the method specific function.

        Returns
        -------
        class:`ElementaryEffect`

        Raises
        ------
        ValueError:
            If method is not valid

        See also
        --------
        func:`sampling.vonneumann_neighbors`
        func:`sampling.trajectory`

        func:`~.elementary_effects_radial`
        func:`~.elementary_effects_trajectory`

    """

    # pre-process arguments & checks
    method = method.lower()
    if method not in ('radial', 'trajectory'):
        raise ValueError(f"Method '{method}' is not supported. Please choose "
                         "'radial' or 'trajectory'.")

    # Generate samples: uses methods
    if method == "radial":
        xs = sampling.vonneumann_neighbors(seeds, delta=delta, **kwargs)
    elif method == "trajectory":
        xs, active_dim = sampling.trajectory(seeds, delta=delta, **kwargs)

    # Evaluate the model independent of chosen method
    ys = model_eval(model, xs)

    # Evaluate elementary effects
    if method == "radial":
        ee = elementary_effects_radial(ys, delta=delta, **kwargs)
    elif method == "trajectory":
        ee = elementary_effects_trajectory(xs, ys, **kwargs)

    return ee


def elementary_effects_radial(ys, delta=None, neighbors='all', delta_tol=1e-8):
    """ Compute the elementary effects of the given model using radial method.

        The method assumes the model was evaluated at the Von Neumann neighbors
        of each seed.

        Arguments
        ---------
        ys: array_like, shape (m, n_seeds)
            Evaluations of the model for each point in the neighborhood.
            m = 2 * x_dim + 1 if whole neighborhood
            m = x_dim + 1 if half neighborhood (same cost as trajectory method)
            x_dim is the number of the input parameters

        delta: float or array_like
            Radius of the neighborhood. If None then unit distance is considered.
            If float, then it's the distance in all dimensions.
            If array_like, then shape must be (x_dim,) indicating the distance
            in each dimension.

        neighbors: string
            define how many of the neighboring points will be used to calculate
            elementary effects
            'all' takes all neighboring points of a seed resulting in
            n_seeds*(2*m+1) calculations
            'half' takes only m neighbors per seed resulting in the same cost
            as the trajectory method n_seeds*(m+1) from one seed.
            Note for 'half': At the moment the neighbors in positive step
            direction are taken.

        delta_tol: float
            Tolerance for the steps in the trajectory.
            Points closer than this will be considered fixed.
            The output of this function is undefined for neighborhoods with a
            distance smaller than this tolerance.

        Returns
        -------
        class:`.ElementaryEffect`

        See also
        --------
        func:`~sampling.vonneumann_neighbors`

        Notes
        -----
        In  "An effective screening design for sensitivity analysis of large models"
        DOI:10.1016/j.envsoft.2006.10.004 the elementary effects are summarized
        using the mean of their absolute values, i.e.

        .. code-block::

            ee = elementary_effects(model, x0)
            mu_ee = np.mean(abs(ee), (0, 2))


    """
    # pre-process arguments & checks
    neigh_choice = ['all', 'half']
    m = ys.shape[0]

    if neighbors == 'all':
        x_dim = int((m - 1) / 2)
        if m % 2 == 0:
            raise ValueError(f"The array of output values has an even number. "
                             f"This is not possible for the radial method with"
                             f" all neighbors.")
    elif neighbors == 'half':
        x_dim = int(m - 1)
    else:
        raise ValueError(f'neighbor value {neighbors} is not available.')

    dys = ys[1:, :] - ys[0, :]
    e_eff = np.zeros_like(dys)

    if neighbors == 'all':
        dim_idx = np.array([[0], [x_dim]]) + list(range(0, x_dim))
        dxs = delta * [[1], [-1]]
        for i in range(x_dim):
            e_eff[dim_idx[:, i], :] = (dys[dim_idx[:, i], :].T / dxs[:, i]).T

        # e_eff dimensions correspond to:
        # direction of change(+, -), step dimension, seed
        e_eff = e_eff[dim_idx[:], :]
        dys = dys[dim_idx[:], :]

        # transpose e_eff to have
        e_eff = np.transpose(e_eff, (0, 2, 1))

    elif neighbors == 'half':
        dxs = delta
        e_eff = dys.T / dxs

    # output format of e_eff
    #   - for 'all': direction of change(+, -), seed, step dimension
    #   - for 'half': seed, step dimension
    return ElementaryEffect(ee=e_eff, delta_x=dxs, delta_y=dys)


def elementary_effects_on_trajectory_unsafe(xt, yt, delta_tol):
    """ Compute the elementary effects on a single trajectory.

        Arguments
        ---------
        xt: array_like, shape (x_dim + 1, x_dim)
            Coordinates of the points on the trajectory in order.
            x_dim is the number of the input parameters.

        yt: array_like, shape (x_dim + 1, )
            Model evaluations of xt.

        delta_tol: float
            lower limit for delta, if delta < delta_tol, the steps will be
            ignored

        Returns
        -------
        dxt: array_like, shape (x_dim, x_dim)
            Differences between one trajectory points to the next.

        dyt: array_like, shape (x_dim, )
            Differences between the model evaluations of the trajectory steps.

        e_eff_t: array_like, shape (x_dim, )
            Elementary effects resulting from this trajectory. (dyt / dxt)


    """
    # difference in model evaluations
    dyt = np.diff(yt)
    e_eff = np.zeros_like(dyt)
    # difference in input vals
    dxt = np.diff(xt, axis=0)
    # read the active dim at each step and eval the el effect
    pa = np.isclose(dxt, 0, atol=delta_tol) == 0
    for point_id, active_dim in enumerate(pa):
        e_eff[active_dim] = (dyt[point_id] / dxt[point_id, active_dim])
    return e_eff, dxt, dyt


def elementary_effects_trajectory(xs, ys, *, delta_tol=1e-8):
    """ Compute the elementary effects of the given model using trajectory method.

        Arguments
        ------------
        xs: array_like, shape (n_seeds, x_dim+1, x_dim)
            Trajectory in parameter space.

        ys: array_like, shape (x_dim + 1, n_seeds)
            Evaluation of the model on each trajectory.

        delta_tol: float
            lower limit for delta, if delta < delta_tol, the steps will be
            ignored

        Returns
        -------
        class:`.ElementaryEffect`

    """

    # Loop over trajectory, evaluate ee on that trajectory, put all ee together
    e_eff = []
    dxs = np.zeros([xs.shape[0], xs.shape[1] - 1, xs.shape[2]])
    dys = np.zeros([ys.shape[0] - 1, ys.shape[1]])
    for n_traj, xt in enumerate(xs):
        yt = ys[:, n_traj]
        e_eff_t, dxt, dyt \
            = elementary_effects_on_trajectory_unsafe(xt, yt,
                                                      delta_tol=delta_tol)
        # store trajectory results
        e_eff.append(e_eff_t)
        dxs[n_traj] = dxt
        dys[:, n_traj] = dyt

    # format ee (trajectory, el effs in parameter sequence)
    return ElementaryEffect(ee=np.array(e_eff), delta_x=dxs, delta_y=dys)
