"""
    Utilities to sample models
"""

# Copyright (C) 2021 Philippe Wiederkehr
#
# This program is free software; you can redistribute it and/or modify it under
# the terms of the GNU General Public License as published by the Free Software
# Foundation; either version 3 of the License, or (at your option) any later
# version.
#
# This program is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
# FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
# details.
#
# You should have received a copy of the GNU General Public License along with
# this program; if not, see <http://www.gnu.org/licenses/>.
#
# Author: Philippe Wiederkehr <philippe_wiederkehr@hotmail.com>

import numpy as np
import math
from numpy.random import default_rng


########################
# Neighborhood (aka radial) sampling
########################
def vonneumann_neighbors(seeds: np.ndarray, delta):
    """ First order von Neumann neighbors of given points.

        Arguments
        ---------
        seeds: array_like, shape (n_seeds, x_dim)
            Centers of the Von Neumann neighbourhoods

        delta: float or array_like
            Radius of the neighborhood. If None then unit distance is considered.
            If float, then it's the distance in all dimensions.
            If array_like, then shape must be (x_dim,) indicating the
            distance in each dimension.

        Returns
        -------
        :class:`numpy.array`
            Coordinates of the points and their neighbors
            shape (n_seeds, {2*x_dim+1 or x_dim+1}, x_dim) with array
            dimensions corresponding to (centers, neighbors, dimensions)

        See also
        --------
        `Wikipedia on Von Neumann neighborhood
        <https://en.wikipedia.org/wiki/Von_Neumann_neighborhood>`_

    """
    n_seeds, x_dim = seeds.shape
    seeds = np.reshape(seeds.T, [1, x_dim, n_seeds])
    neighborhood = vonneumann_neighborhood(x_dim, delta=delta)
    n = np.zeros([neighborhood.shape[0], x_dim, 1])
    n[:, :, 0] = neighborhood
    n = seeds + n
    # format of output: seeds, neighbors, dimensions
    return n.transpose((2, 0, 1))


def vonneumann_neighborhood(x_dim, delta=None):
    """ First order Von Neumann neighborhood around 0.

        The von Neumann neighbourhood of a cell is the cell itself and the
        cells at a Manhattan distance of 1. In this function, the distance can
        be set to a value other than 1 or specifically for each dimension.

        Arguments
        ---------
        x_dim: int
            Dimension of the neighbourhood.

        delta: float or array_like
            Radius of the neighborhood. If None then unit distance is considered.
            If float, then is the distance in all directions.
            If array_like, then shape must be (x_dim,) indicating the distance
            on each direction.

        Returns
        -------
        :class:`numpy.array`
            Coordinates of the centerpoint 0 and its neighborhood
            shape ({x_dim+1 or 2*x_dim+1}, x_dim) with array
            dimensions corresponding to (neighbors, dimensions)

        See also
        ----------
        :func:`~.vonneumann_neighbors`

        `Wikipedia on Von Neumann neighborhood <https://en.wikipedia.org/wiki/Von_Neumann_neighborhood>`_

        Example
        -------
        >>> vonneumann_neighborhood(2)
        array([[ 0.,  0.],
               [ 1.,  0.],
               [ 0.,  1.],
               [-1., -0.],
               [-0., -1.]])
        >>> vonneumann_neighborhood(2, delta=0.1)
        array([[ 0. ,  0. ],
               [ 0.1,  0. ],
               [ 0. ,  0.1],
               [-0.1, -0. ],
               [-0. , -0.1]])
        >>> vonneumann_neighborhood(2, delta=[0.1, 2])
        array([[ 0. ,  0. ],
               [ 0.1,  0. ],
               [ 0. ,  2. ],
               [-0.1, -0. ],
               [-0. , -2. ]])
    """
    # param checks
    delta = np.ones(x_dim) if delta is None else np.asarray(delta)
    if delta.size <= 1:
        delta = np.ones(x_dim) * delta
    elif delta.size != x_dim:
        raise ValueError(f'length of delta does not match provided x_dim')

    # construct neighborhood
    idt = np.eye(x_dim)
    hood = np.concatenate((np.zeros([1, x_dim]), idt, -idt))

    return hood * delta


########################
# Trajectory (aka path) sampling
########################
def trajectory(seeds, *, delta, **kwargs):
    """ Trajectories through the input space by varying one coordinate at a time.

        Arguments
        ---------
        seeds: array_like, shape (n_seeds, x_dim)
            Starting points of the trajectories

        delta: array_like
            Trajectory step widths. If None then unit step width is considered.
            Shape must be (x_dim,) indicating the step width in each direction.

        Returns
        -------
        :class:`numpy.array`
           Coordinates of the trajectory points
           shape (n_seeds, x_dim+1, x_dim) with array dimensions corresponding
           to (trajectories, trajectory steps, dimensions)


        See also
        --------
        :func:`~.random_path`
        :func:`~.best_trajectory_set`
        `Wikipedia on Morris elementary effects method <https://en.wikipedia.org/wiki/Elementary_effects_method>`_
        `An effective screening design for sensitivity analysis of large models <https://doi.org/10.1016/j.envsoft.2006.10.004>`_

        Note
        ----
        A basic version of the trajectory method is implemented
        here with one step per dimension per trajectory. The trajectories
        could be continued indefinitely by adding
        steps in random directions as long as the resulting points stay
        inside the sampling space.

    """

    # Preprocess and checks
    x_dim = len(delta)

    # preprocess & preallocate
    n_seeds, x_dim = seeds.shape
    trajectories = np.zeros([n_seeds, x_dim + 1, x_dim])
    active_dims = np.zeros([n_seeds, x_dim])
    # generate a random trajectory for each seed
    for ii, curr_seed in enumerate(seeds):
        # curr_seed = np.reshape(seeds.T, [1, x_dim, n_seeds])
        # path = np.zeros([x_dim + 1, x_dim, 1])
        path, active_dims[ii, :] = random_path(x_dim, delta)
        # attach path to starting point and store as a trajectory,
        # format (trajectories, steps, dimensions)
        trajectories[ii, :, :] = curr_seed + path

    # format of trajectories: trajectories, steps, dimensions
    return trajectories, active_dims


def random_path(x_dim: int, delta=None):
    """ Builds a random trajectory through the input space starting at 0 by
    stepping in all dimension successively.

         Arguments
         ---------
         x_dim: integer
             number of dimensions to step through

         delta: float or array_like
             Trajectory step widths. If None then unit step width is considered.
             If float, then is the step width in all directions.
             If array_like, then shape must be (x_dim,) indicating the step width on each direction.

         Returns
         -------
         :class:`numpy.array`
             Coordinates of the trajectory points starting at 0, shape (x_dim+1, x_dim)

         See also
         --------
         :func:`~.trajectory`

     """
    # steps in order
    steps = np.eye(x_dim) * delta

    # get random step sequence
    index_list = list(range(x_dim))
    rand_sequence = np.random.permutation(index_list)

    # add zero as the starting point
    random_steps = np.concatenate((np.zeros([1, x_dim]), steps[rand_sequence, :]))

    # add each the step to the coordinates before
    for i in range(1, x_dim + 1):
        random_steps[i, :] = np.sum(random_steps[i - 1:i + 1, :], 0)

    # format of random_steps: steps, dimensions. Shape is (x_dim+1, x_dim).
    # rand_sequence is needed to calc the elementary effects later
    return random_steps, rand_sequence
