"""
    General tools
"""

# Copyright (C) 2021 Juan Pablo Carbajal
#
# This program is free software; you can redistribute it and/or modify it under
# the terms of the GNU General Public License as published by the Free Software
# Foundation; either version 3 of the License, or (at your option) any later
# version.
#
# This program is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
# FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
# details.
#
# You should have received a copy of the GNU General Public License along with
# this program; if not, see <http://www.gnu.org/licenses/>.

# Author: Juan Pablo Carbajal <ajuanpi+dev@gmail.com>

from collections import namedtuple
import numpy as np
import pandas as pd


def model_eval(model, xs):
    """ Evaluate the model independent of chosen sampling method

        Arguments
        ---------
        model: callable
            The scalar model function. Signature?

        xs: array_like, shape (n_seeds, m, x_dim)
            The input values on which to evaluate the model
            The first dimension is the number of seeds used in the method.
            The second dimension is given by the sampling method chosen.
            The last dimensions is the dimension of the input space.

        Returns
        -------
        :class:`np.ndarray`, shape (m, n_seeds)
            Evaluation of the model at each sample
    """
    n_seeds, m, x_dim = xs.shape
    # Reshape to get all points with their steps below each other
    xs_ = np.reshape(xs, (m * n_seeds, x_dim))

    # Eval model and reshape back
    ys = np.reshape(model(xs_), (n_seeds, m)).T

    return ys


def results_to_df(results, method):
    """ Turn the elementary effect from ndarray to a pandas DataFrame

        Arguments
        ---------
        results: array-like
            The elementary effect field of the namedTuple calculated by the
            module.

        method: string ('radial' or 'trajectory')
            The method used to calculate the elementary effects.

        Returns
        -------
        :class:`pd.DataFrame`
            Elementary effects in DataFrame form
    """
    def variable_names(n_var):
        return [f'x_{n}' for n in range(n_var)]

    if method == 'radial':
        n_dir, n_ee, n_var = results.shape
    if method == 'trajectory':
        n_ee, n_var = results.shape
        n_dir = 1

    df_ee = pd.DataFrame(results.reshape(n_dir * n_var * n_ee, order='F'),
                         columns=['elementary_effect'])
    if method == 'radial':
        # radial method has a direction, add column with this information
        df_ee['step_direction'] = np.array((['pos', 'neg'] * n_var * n_ee))

    var_names = variable_names(n_var)
    var_col = np.repeat(var_names, n_dir * n_ee)
    df_ee['variable'] = var_col
    df_ee['method'] = method

    return df_ee


def mu_star(x):
    """ Mean of absolute values. """
    return np.mean(np.abs(x))
